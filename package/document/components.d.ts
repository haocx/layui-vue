// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/vue-next/pull/3399
import "@vue/runtime-core";

declare module "@vue/runtime-core" {
  export interface GlobalComponents {
    Animation: typeof import("./src/document/zh-CN/components/animation.md")["default"];
    Avatar: typeof import("./src/document/zh-CN/components/avatar.md")["default"];
    Backtop: typeof import("./src/document/zh-CN/components/backtop.md")["default"];
    Badge: typeof import("./src/document/zh-CN/components/badge.md")["default"];
    Breadcrumb: typeof import("./src/document/zh-CN/components/breadcrumb.md")["default"];
    Button: typeof import("./src/document/zh-CN/components/button.md")["default"];
    Card: typeof import("./src/document/zh-CN/components/card.md")["default"];
    Carousel: typeof import("./src/document/zh-CN/components/carousel.md")["default"];
    Checkbox: typeof import("./src/document/zh-CN/components/checkbox.md")["default"];
    Collapse: typeof import("./src/document/zh-CN/components/collapse.md")["default"];
    Color: typeof import("./src/document/zh-CN/components/color.md")["default"];
    ColorPicker: typeof import("./src/document/zh-CN/components/colorPicker.md")["default"];
    Confirm: typeof import("./src/document/zh-CN/components/confirm.md")["default"];
    Container: typeof import("./src/document/zh-CN/components/container.md")["default"];
    Countup: typeof import("./src/document/zh-CN/components/countup.md")["default"];
    DatePicker: typeof import("./src/document/zh-CN/components/datePicker.md")["default"];
    Drawer: typeof import("./src/document/zh-CN/components/drawer.md")["default"];
    Dropdown: typeof import("./src/document/zh-CN/components/dropdown.md")["default"];
    Empty: typeof import("./src/document/zh-CN/components/empty.md")["default"];
    Exception: typeof import("./src/document/zh-CN/components/exception.md")["default"];
    Field: typeof import("./src/document/zh-CN/components/field.md")["default"];
    Form: typeof import("./src/document/zh-CN/components/form.md")["default"];
    Fullscreen: typeof import("./src/document/zh-CN/components/fullscreen.md")["default"];
    Grid: typeof import("./src/document/zh-CN/components/grid.md")["default"];
    Icon: typeof import("./src/document/zh-CN/components/icon.md")["default"];
    IconPicker: typeof import("./src/document/zh-CN/components/iconPicker.md")["default"];
    Input: typeof import("./src/document/zh-CN/components/input.md")["default"];
    InputNumber: typeof import("./src/document/zh-CN/components/inputNumber.md")["default"];
    LayAvatar: typeof import("@layui/layui-vue")["LayAvatar"];
    LayAvatarList: typeof import("@layui/layui-vue")["LayAvatarList"];
    LayBacktop: typeof import("@layui/layui-vue")["LayBacktop"];
    LayBadge: typeof import("@layui/layui-vue")["LayBadge"];
    LayBody: typeof import("@layui/layui-vue")["LayBody"];
    LayBreadcrumb: typeof import("@layui/layui-vue")["LayBreadcrumb"];
    LayBreadcrumbItem: typeof import("@layui/layui-vue")["LayBreadcrumbItem"];
    LayButton: typeof import("@layui/layui-vue")["LayButton"];
    LayButtonContainer: typeof import("@layui/layui-vue")["LayButtonContainer"];
    LayButtonGroup: typeof import("@layui/layui-vue")["LayButtonGroup"];
    LayCard: typeof import("@layui/layui-vue")["LayCard"];
    LayCarousel: typeof import("@layui/layui-vue")["LayCarousel"];
    LayCarouselItem: typeof import("@layui/layui-vue")["LayCarouselItem"];
    LayCheckbox: typeof import("@layui/layui-vue")["LayCheckbox"];
    LayCheckboxGroup: typeof import("@layui/layui-vue")["LayCheckboxGroup"];
    LayCol: typeof import("@layui/layui-vue")["LayCol"];
    LayCollapse: typeof import("@layui/layui-vue")["LayCollapse"];
    LayCollapseItem: typeof import("@layui/layui-vue")["LayCollapseItem"];
    LayColorPicker: typeof import("@layui/layui-vue")["LayColorPicker"];
    LayConfigProvider: typeof import("@layui/layui-vue")["LayConfigProvider"];
    LayContainer: typeof import("@layui/layui-vue")["LayContainer"];
    LayCountUp: typeof import("@layui/layui-vue")["LayCountUp"];
    LayDropdown: typeof import("@layui/layui-vue")["LayDropdown"];
    LayDropdownMenu: typeof import("@layui/layui-vue")["LayDropdownMenu"];
    LayDropdownMenuItem: typeof import("@layui/layui-vue")["LayDropdownMenuItem"];
    LayEmpty: typeof import("@layui/layui-vue")["LayEmpty"];
    Layer: typeof import("./src/document/zh-CN/components/layer.md")["default"];
    LayException: typeof import("@layui/layui-vue")["LayException"];
    LayField: typeof import("@layui/layui-vue")["LayField"];
    LayFooter: typeof import("@layui/layui-vue")["LayFooter"];
    LayForm: typeof import("@layui/layui-vue")["LayForm"];
    LayFormItem: typeof import("@layui/layui-vue")["LayFormItem"];
    LayFullscreen: typeof import("@layui/layui-vue")["LayFullscreen"];
    LayHeader: typeof import("@layui/layui-vue")["LayHeader"];
    LayIcon: typeof import("@layui/icons-vue")["LayIcon"];
    LayIconPicker: typeof import("@layui/layui-vue")["LayIconPicker"];
    LayInput: typeof import("@layui/layui-vue")["LayInput"];
    LayInputNumber: typeof import("@layui/layui-vue")["LayInputNumber"];
    LayLayer: typeof import("@layui/layer-vue")["LayLayer"];
    LayLayout: typeof import("@layui/layui-vue")["LayLayout"];
    LayLine: typeof import("@layui/layui-vue")["LayLine"];
    LayLogo: typeof import("@layui/layui-vue")["LayLogo"];
    LayMenu: typeof import("@layui/layui-vue")["LayMenu"];
    LayMenuItem: typeof import("@layui/layui-vue")["LayMenuItem"];
    LayNoticeBar: typeof import("@layui/layui-vue")["LayNoticeBar"];
    Layout: typeof import("./src/document/zh-CN/components/layout.md")["default"];
    LayPage: typeof import("@layui/layui-vue")["LayPage"];
    LayPanel: typeof import("@layui/layui-vue")["LayPanel"];
    LayProgress: typeof import("@layui/layui-vue")["LayProgress"];
    LayQuote: typeof import("@layui/layui-vue")["LayQuote"];
    LayRadio: typeof import("@layui/layui-vue")["LayRadio"];
    LayRate: typeof import("@layui/layui-vue")["LayRate"];
    LayResult: typeof import("@layui/layui-vue")["LayResult"];
    LayRipple: typeof import("@layui/layui-vue")["LayRipple"];
    LayRow: typeof import("@layui/layui-vue")["LayRow"];
    LayScroll: typeof import("@layui/layui-vue")["LayScroll"];
    LaySelect: typeof import("@layui/layui-vue")["LaySelect"];
    LaySelectOption: typeof import("@layui/layui-vue")["LaySelectOption"];
    LaySide: typeof import("@layui/layui-vue")["LaySide"];
    LaySkeleton: typeof import("@layui/layui-vue")["LaySkeleton"];
    LaySkeletonItem: typeof import("@layui/layui-vue")["LaySkeletonItem"];
    LaySlider: typeof import("@layui/layui-vue")["LaySlider"];
    LaySplitPanel: typeof import("@layui/layui-vue")["LaySplitPanel"];
    LaySplitPanelItem: typeof import("@layui/layui-vue")["LaySplitPanelItem"];
    LayStep: typeof import("@layui/layui-vue")["LayStep"];
    LayStepItem: typeof import("@layui/layui-vue")["LayStepItem"];
    LaySubMenu: typeof import("@layui/layui-vue")["LaySubMenu"];
    LaySwitch: typeof import("@layui/layui-vue")["LaySwitch"];
    LayTab: typeof import("@layui/layui-vue")["LayTab"];
    LayTabItem: typeof import("@layui/layui-vue")["LayTabItem"];
    LayTable: typeof import("@layui/layui-vue")["LayTable"];
    LayTextarea: typeof import("@layui/layui-vue")["LayTextarea"];
    LayTimeline: typeof import("@layui/layui-vue")["LayTimeline"];
    LayTimelineItem: typeof import("@layui/layui-vue")["LayTimelineItem"];
    LayTooltip: typeof import("@layui/layui-vue")["LayTooltip"];
    LayTransfer: typeof import("@layui/layui-vue")["LayTransfer"];
    LayTransition: typeof import("@layui/layui-vue")["LayTransition"];
    LayTree: typeof import("@layui/layui-vue")["LayTree"];
    LayUpload: typeof import("@layui/layui-vue")["LayUpload"];
    Line: typeof import("./src/document/zh-CN/components/line.md")["default"];
    Load: typeof import("./src/document/zh-CN/components/load.md")["default"];
    Menu: typeof import("./src/document/zh-CN/components/menu.md")["default"];
    Modal: typeof import("./src/document/zh-CN/components/modal.md")["default"];
    Msg: typeof import("./src/document/zh-CN/components/msg.md")["default"];
    NoticeBar: typeof import("./src/document/zh-CN/components/noticeBar.md")["default"];
    Page: typeof import("./src/document/zh-CN/components/page.md")["default"];
    Panel: typeof import("./src/document/zh-CN/components/panel.md")["default"];
    Progress: typeof import("./src/document/zh-CN/components/progress.md")["default"];
    Quote: typeof import("./src/document/zh-CN/components/quote.md")["default"];
    Radio: typeof import("./src/document/zh-CN/components/radio.md")["default"];
    Rate: typeof import("./src/document/zh-CN/components/rate.md")["default"];
    Result: typeof import("./src/document/zh-CN/components/result.md")["default"];
    Ripple: typeof import("./src/document/zh-CN/components/ripple.md")["default"];
    RouterLink: typeof import("vue-router")["RouterLink"];
    RouterView: typeof import("vue-router")["RouterView"];
    Scroll: typeof import("./src/document/zh-CN/components/scroll.md")["default"];
    Select: typeof import("./src/document/zh-CN/components/select.md")["default"];
    Skeleton: typeof import("./src/document/zh-CN/components/skeleton.md")["default"];
    Slider: typeof import("./src/document/zh-CN/components/slider.md")["default"];
    SplitPanel: typeof import("./src/document/zh-CN/components/splitPanel.md")["default"];
    Step: typeof import("./src/document/zh-CN/components/step.md")["default"];
    Switch: typeof import("./src/document/zh-CN/components/switch.md")["default"];
    Tab: typeof import("./src/document/zh-CN/components/tab.md")["default"];
    Table: typeof import("./src/document/zh-CN/components/table.md")["default"];
    Textarea: typeof import("./src/document/zh-CN/components/textarea.md")["default"];
    Timeline: typeof import("./src/document/zh-CN/components/timeline.md")["default"];
    Tooltip: typeof import("./src/document/zh-CN/components/tooltip.md")["default"];
    Transfer: typeof import("./src/document/zh-CN/components/transfer.md")["default"];
    Transition: typeof import("./src/document/zh-CN/components/transition.md")["default"];
    Tree: typeof import("./src/document/zh-CN/components/tree.md")["default"];
    Upload: typeof import("./src/document/zh-CN/components/upload.md")["default"];
  }
}

export {};
